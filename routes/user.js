const router = require('express').Router();
const { check, validationResult } = require('express-validator');
const shajs = require('sha.js');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const { authorizeUser } = require('../utils/auth');
require('dotenv').config();

router.post(
  '/register',
  [
    check('email').isEmail(),
    check('password').isLength({ min: 8, max: 32 }),
    check('nickname').isLength({ min: 2, max: 32 }),
  ],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errors);
    }

    const passwordHash = shajs('sha256')
      .update(req.body.password)
      .digest('hex');

    const user = new User({
      email: req.body.email,
      password: passwordHash,
      nickname: req.body.nickname,
    });

    user
      .save()
      .then(registeredUser => {
        const token = jwt.sign({ _id: registeredUser._id }, process.env.SECRET);
        return res.status(201).json({
          message: 'Successfully reigstrated user!',
          token,
          user: registeredUser,
        });
      })
      .catch(err => {
        console.log(err);
        return res.status(400).json({
          error:
            err.code === 11000
              ? 'This email is already taken'
              : 'Error while creating account. Try again',
        });
      });
  }
);

router.post(
  '/login',
  [check('email').isEmail(), check('password').isLength({ min: 8, max: 32 })],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errors);
    }

    User.findOne({ email: req.body.email }).then(user => {
      if (!user) {
        return res.status(400).json({ error: 'No user found with this email' });
      }

      const passwordHash = shajs('sha256')
        .update(req.body.password)
        .digest('hex');

      if (user.password === passwordHash) {
        const token = jwt.sign({ _id: user._id }, process.env.SECRET);
        return res.status(201).json({
          message: 'Successfully logged in!',
          token,
          user,
        });
      } else {
        return res.status(400).json({ error: 'Incorrect password' });
      }
    });
  }
);

router.get('/', authorizeUser, (req, res) => {
  res.json({ user: req.user });
});

module.exports = router;
