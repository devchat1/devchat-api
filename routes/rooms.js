const router = require('express').Router();
const { check, validationResult } = require('express-validator');
const { authorizeUser, authorizeUserSocket } = require('../utils/auth');
const mongoose = require('mongoose');
const { Room, Message } = require('../models/Room');

router.post('/new', authorizeUser, [check('title').isLength({ min: 3, max: 32 })], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json(errors);
  }

  const room = new Room({ title: req.body.title, members: [req.user], messages: [] });

  room.save().then(createdRoom => res.json({ message: 'Room created', room: createdRoom }));
});

router.get('/:id', authorizeUser, (req, res) => {
  if (!req.params.id) {
    return res.status(400).json('Pass the room id');
  }
  if (!req.query.number) {
    return res.status(400).json('Give the number of saved messages');
  }

  Room.aggregate([
    {
      $match: {
        $and: [
          {
            _id: mongoose.Types.ObjectId(req.params.id),
          },
        ],
      },
    },
    {
      $project: {
        messages: {
          $slice: [
            '$messages',
            parseInt(req.query.number),
            {
              $subtract: [
                {
                  $size: '$messages',
                },
                parseInt(req.query.number),
              ],
            },
          ],
        },
      },
    },
  ])
    .then(room => {
      if (!room[0]) {
        return res.json({
          message: `Can't find room with given ID`,
        });
      }
      Room.populate(room[0], ['messages.from']).then(messages => {
        return res.json(messages);
      });
    })
    .catch(() => {
      return res.json({ message: 'No new messages' });
    });
});

const io = http => {
  const io = require('socket.io')(http, { serveClient: false });

  io.use(authorizeUserSocket);

  io.on('connection', socket => {
    socket.join(`/${socket.handshake.query.room_id}`);

    socket.on('message sent', msg => {
      Room.findById(socket.handshake.query.room_id).then(room => {
        if (room) {
          if (msg.content.length > 0 && msg.contentType) {
            const newMessage = new Message({
              content: msg.content,
              contentType: msg.contentType,
              from: socket.user,
              time: new Date(),
            });

            room.messages.push(newMessage);

            room.save().then(() => {
              io.to(`/${socket.handshake.query.room_id}`).emit('message', newMessage);
            });
          }
        }
      });
    });
  });
};

module.exports = {
  router,
  io,
};
