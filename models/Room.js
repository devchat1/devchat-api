const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
  content: {
    type: String,
  },
  contentType: {
    type: String,
  },
  from: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  time: {
    type: Date,
  },
});

const roomSchema = new mongoose.Schema({
  messages: [messageSchema],
  members: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
  ],
  title: String,
});

module.exports = {
  Room: mongoose.model('Room', roomSchema),
  Message: mongoose.model('Message', messageSchema),
};
