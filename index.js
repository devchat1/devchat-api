const app = require('express')();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const http = require('http').createServer(app);
require('dotenv').config();

mongoose.connect(process.env.MONGO_CRED).then(() => console.log('Database connected'));

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());
app.use(cors());

app.get('/', (req, res) => res.json({ message: 'Welcome to devchat api' }));

const userRoute = require('./routes/user');
const roomsRoute = require('./routes/rooms').router;

app.use('/user', userRoute);
app.use('/rooms', roomsRoute);

require('./routes/rooms').io(http);

const port = process.env.PORT || 8080;
http.listen(port, () => console.log(`Server listening at port ${port}`));
