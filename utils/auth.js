const jwt = require('jsonwebtoken');
const User = require('../models/User');
require('dotenv').config();

const authorizeUser = (req, res, next) => {
  if (!req.headers.authorization) return res.status(401).json({ error: 'No login token passed' });

  let jwtObject;
  try {
    jwtObject = jwt.verify(req.headers.authorization.replace('Bearer ', ''), process.env.SECRET);
  } catch {
    return res.status(401).json({ error: 'Invalid login credentials' });
  }

  User.findById(jwtObject._id).then(user => {
    if (!user) {
      return res.status(401).json({ error: 'Invalid login credentials' });
    } else {
      req.user = user;
      next();
    }
  });
};

const authorizeUserSocket = (socket, next) => {
  const token = socket.handshake.query.token;

  if (token) {
    const jwtObject = jwt.verify(token, process.env.SECRET);

    User.findById(jwtObject._id).then(user => {
      if (!user) {
        return next(new Error('Invalid login credentials'));
      } else {
        socket.user = user;
        return next();
      }
    });
  } else {
    return next(new Error('Invalid login credentials'));
  }
};

module.exports = {
  authorizeUser,
  authorizeUserSocket,
};
